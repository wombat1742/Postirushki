USE [Postirushki]
GO
/****** Object:  Table [dbo].[Branch]    Script Date: 29.04.2023 18:41:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Branch](
	[BranchId] [int] NOT NULL,
	[BranchName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Branch] PRIMARY KEY CLUSTERED 
(
	[BranchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Machine]    Script Date: 29.04.2023 18:41:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Machine](
	[MachineId] [int] NOT NULL,
	[MachineStatus] [int] NOT NULL,
	[MachineBranch] [int] NULL,
 CONSTRAINT [PK_Machine] PRIMARY KEY CLUSTERED 
(
	[MachineId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MachineStatus]    Script Date: 29.04.2023 18:41:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MachineStatus](
	[Id] [int] NOT NULL,
	[MachineStatusName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_MachineStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 29.04.2023 18:41:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[OrderId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[MachineId] [int] NOT NULL,
	[Drying] [tinyint] NOT NULL,
	[PowderType] [int] NOT NULL,
	[StatusId] [int] NOT NULL,
	[WashDate] [date] NOT NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PowderType]    Script Date: 29.04.2023 18:41:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PowderType](
	[PowderTypeId] [int] NOT NULL,
	[PowderTypeName] [nchar](10) NOT NULL,
	[Price] [int] NOT NULL,
 CONSTRAINT [PK_PowderType] PRIMARY KEY CLUSTERED 
(
	[PowderTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 29.04.2023 18:41:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[RoleId] [int] NOT NULL,
	[RoleName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 29.04.2023 18:41:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[StatusId] [int] NOT NULL,
	[StatusName] [nchar](10) NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 29.04.2023 18:41:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Patronym] [nvarchar](50) NULL,
	[Login] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[RoleId] [int] NULL,
	[BranchId] [int] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Branch] ([BranchId], [BranchName]) VALUES (1, NULL)
INSERT [dbo].[Branch] ([BranchId], [BranchName]) VALUES (2, NULL)
INSERT [dbo].[Branch] ([BranchId], [BranchName]) VALUES (3, NULL)
INSERT [dbo].[Branch] ([BranchId], [BranchName]) VALUES (4, NULL)
GO
INSERT [dbo].[Machine] ([MachineId], [MachineStatus], [MachineBranch]) VALUES (1, 0, 3)
INSERT [dbo].[Machine] ([MachineId], [MachineStatus], [MachineBranch]) VALUES (2, 0, 2)
INSERT [dbo].[Machine] ([MachineId], [MachineStatus], [MachineBranch]) VALUES (3, 0, 2)
INSERT [dbo].[Machine] ([MachineId], [MachineStatus], [MachineBranch]) VALUES (4, 0, 2)
INSERT [dbo].[Machine] ([MachineId], [MachineStatus], [MachineBranch]) VALUES (5, 0, 2)
INSERT [dbo].[Machine] ([MachineId], [MachineStatus], [MachineBranch]) VALUES (6, 0, 3)
INSERT [dbo].[Machine] ([MachineId], [MachineStatus], [MachineBranch]) VALUES (7, 0, 3)
INSERT [dbo].[Machine] ([MachineId], [MachineStatus], [MachineBranch]) VALUES (8, 0, 2)
INSERT [dbo].[Machine] ([MachineId], [MachineStatus], [MachineBranch]) VALUES (9, 1, 2)
INSERT [dbo].[Machine] ([MachineId], [MachineStatus], [MachineBranch]) VALUES (10, 0, NULL)
GO
INSERT [dbo].[MachineStatus] ([Id], [MachineStatusName]) VALUES (0, N'Свободна')
INSERT [dbo].[MachineStatus] ([Id], [MachineStatusName]) VALUES (1, N'Занята')
GO
INSERT [dbo].[Order] ([OrderId], [UserId], [MachineId], [Drying], [PowderType], [StatusId], [WashDate]) VALUES (1, 1, 3, 1, 2, 2, CAST(N'2021-09-19' AS Date))
INSERT [dbo].[Order] ([OrderId], [UserId], [MachineId], [Drying], [PowderType], [StatusId], [WashDate]) VALUES (2, 2, 8, 0, 3, 2, CAST(N'2021-09-19' AS Date))
INSERT [dbo].[Order] ([OrderId], [UserId], [MachineId], [Drying], [PowderType], [StatusId], [WashDate]) VALUES (3, 3, 5, 1, 1, 3, CAST(N'2021-09-19' AS Date))
INSERT [dbo].[Order] ([OrderId], [UserId], [MachineId], [Drying], [PowderType], [StatusId], [WashDate]) VALUES (4, 4, 4, 1, 3, 1, CAST(N'2021-09-19' AS Date))
INSERT [dbo].[Order] ([OrderId], [UserId], [MachineId], [Drying], [PowderType], [StatusId], [WashDate]) VALUES (5, 1, 4, 0, 1, 3, CAST(N'2021-09-19' AS Date))
INSERT [dbo].[Order] ([OrderId], [UserId], [MachineId], [Drying], [PowderType], [StatusId], [WashDate]) VALUES (6, 1, 2, 0, 3, 3, CAST(N'2021-09-19' AS Date))
INSERT [dbo].[Order] ([OrderId], [UserId], [MachineId], [Drying], [PowderType], [StatusId], [WashDate]) VALUES (7, 1, 9, 0, 2, 1, CAST(N'2021-09-19' AS Date))
GO
INSERT [dbo].[PowderType] ([PowderTypeId], [PowderTypeName], [Price]) VALUES (1, N'Tide      ', 35)
INSERT [dbo].[PowderType] ([PowderTypeId], [PowderTypeName], [Price]) VALUES (2, N'Ariel     ', 25)
INSERT [dbo].[PowderType] ([PowderTypeId], [PowderTypeName], [Price]) VALUES (3, N'Persil    ', 30)
GO
INSERT [dbo].[Role] ([RoleId], [RoleName]) VALUES (1, N'Администратор')
INSERT [dbo].[Role] ([RoleId], [RoleName]) VALUES (2, N'Сотрудник')
GO
INSERT [dbo].[Status] ([StatusId], [StatusName]) VALUES (1, N'Стирается ')
INSERT [dbo].[Status] ([StatusId], [StatusName]) VALUES (2, N'Готов     ')
INSERT [dbo].[Status] ([StatusId], [StatusName]) VALUES (3, N'Выдан     ')
GO
INSERT [dbo].[User] ([UserId], [Surname], [Name], [Patronym], [Login], [Password], [RoleId], [BranchId]) VALUES (1, N'Ковалёва', N'Алиса', N'Георгиевна', N'kag', N'skhegi', 2, 2)
INSERT [dbo].[User] ([UserId], [Surname], [Name], [Patronym], [Login], [Password], [RoleId], [BranchId]) VALUES (2, N'Гущина', N'Антонина', N'Матвеевна', N'gam', N'frowth', 2, 2)
INSERT [dbo].[User] ([UserId], [Surname], [Name], [Patronym], [Login], [Password], [RoleId], [BranchId]) VALUES (3, N'Панфилова', N'Юстина', N'Никитевна', N'pun', N'luasgm', 2, 2)
INSERT [dbo].[User] ([UserId], [Surname], [Name], [Patronym], [Login], [Password], [RoleId], [BranchId]) VALUES (4, N'Дроздова', N'Радмила', N'Станиславовна', N'drs', N'ntxjpw', 2, 2)
INSERT [dbo].[User] ([UserId], [Surname], [Name], [Patronym], [Login], [Password], [RoleId], [BranchId]) VALUES (5, N'Воронова', N'Юланта', N'Всеволодовна', N'vuv', N'irzsln', 2, 2)
INSERT [dbo].[User] ([UserId], [Surname], [Name], [Patronym], [Login], [Password], [RoleId], [BranchId]) VALUES (6, N'Орлова', N'Ксения', N'Александровна', N'oka', N'nxfvyl', 2, 3)
INSERT [dbo].[User] ([UserId], [Surname], [Name], [Patronym], [Login], [Password], [RoleId], [BranchId]) VALUES (7, N'Носкова', N'Дания', N'Петровна', N'ndp', N'bxqdpa', 1, 2)
INSERT [dbo].[User] ([UserId], [Surname], [Name], [Patronym], [Login], [Password], [RoleId], [BranchId]) VALUES (8, N'Кулагина', N'Александра', N'Якововна', N'kay', N'ilswvx', 2, 2)
INSERT [dbo].[User] ([UserId], [Surname], [Name], [Patronym], [Login], [Password], [RoleId], [BranchId]) VALUES (9, N'Некрасова', N'Мария', N'Лаврентьевна', N'nml', N'aimgut', 2, 3)
INSERT [dbo].[User] ([UserId], [Surname], [Name], [Patronym], [Login], [Password], [RoleId], [BranchId]) VALUES (10, N'Кулагина', N'Беатриса', N'Рудольфовна', N'kbr', N'sgnvmm', 2, 3)
INSERT [dbo].[User] ([UserId], [Surname], [Name], [Patronym], [Login], [Password], [RoleId], [BranchId]) VALUES (11, N'Беспалова', N'Жюли', N'Евсеевна', N'bge', N'nuexgq', 2, 3)
INSERT [dbo].[User] ([UserId], [Surname], [Name], [Patronym], [Login], [Password], [RoleId], [BranchId]) VALUES (12, N'Иванов', N'Иван', N'Иванович', N'iii', N'hgfdsa', 1, NULL)
GO
ALTER TABLE [dbo].[Machine]  WITH NOCHECK ADD  CONSTRAINT [FK_Machine_MachineStatus] FOREIGN KEY([MachineStatus])
REFERENCES [dbo].[MachineStatus] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Machine] CHECK CONSTRAINT [FK_Machine_MachineStatus]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Machine] FOREIGN KEY([MachineId])
REFERENCES [dbo].[Machine] ([MachineId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Machine]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_PowderType] FOREIGN KEY([PowderType])
REFERENCES [dbo].[PowderType] ([PowderTypeId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_PowderType]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Status] FOREIGN KEY([StatusId])
REFERENCES [dbo].[Status] ([StatusId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Status]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_User]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Branch] FOREIGN KEY([BranchId])
REFERENCES [dbo].[Branch] ([BranchId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Branch]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([RoleId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Role]
GO
